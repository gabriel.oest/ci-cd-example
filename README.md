# CI/CD Kata
## Introduction to CI/CD in Gitlab

- Image
- Stages
- Jobs
    - Name
    - Stage
    - Script
    - Environment
    - Variables
    - Cache
    - Control

## Example

```sh
Name:
  stage: deploy
  environment: development
  script:
    - echo "Hello $USR_NAME..."
  only: 
    - main
```

## Step by Step

### Create the CI/CD configuration file on the root of the project: 
```sh
.gitlab-ci.yml
```

### Define the pipeline image:
```sh
  image: gradle:alpine
```

### Add a dummy Job, commit and push:
```sh
Dummy:
  stage: build
  script:
    - echo "Hello World!"
  only: 
    - main
```

### Add a variable locally on the script
```sh
variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
```

### Add a command to be executed before each job using `before_script`
```sh
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
```

### Add a Build Job
```sh
build:
  stage: build
  script: gradle --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle
```

### Add a Test Job, commit and push it:
```sh
test:
  stage: test
  script: gradle check
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle
```

### Let´s create a failing test and push it to see what happens
```sh
Assertions.assertTrue(false);
```

### Create project environments
```sh
Deployments -> Environments
  dev
  stg
  prd
```

### Add some example variables on the project configuration using the environments
```sh
Settings -> CI/CD -> Variables
  Create an ENV_NAME variable with the environment name as value for each created environment
```

### Create a deploy job for `development` and use the environment created(dev) and the variable(ENV_NAME) on the script. Push it:
```sh
Deploy-Development:
  stage: deploy
  environment: dev
  script:
    - echo "Deploying $ENV_NAME..."
  only: 
    - main
```

### Create a deploy job for `staging` that executes only when a `TAG` is created:
```sh
Deploy-Staging:
  stage: deploy
  environment: stg
  script:
    - echo "Deploying $ENV_NAME..."
  only: 
    - tags
```

### Create a deploy job for `production` that depends of the `staging` job and that executes `manually`
```sh
Deploy-Production:
  stage: deploy
  environment: prd
  needs: 
    - job: Deploy-Staging
  script:
    - echo "Deploying $ENV_NAME..."
  when: manual
  only: 
    - tags
```
